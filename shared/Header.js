import { Image, StyleSheet, View } from "react-native";
function Header() {
  <View style={style.imageContainer}>
    <Image source={require("../assets/logo.jpg")} />;
  </View>;
}
export default Header;

const style = StyleSheet.creat({
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    width: "100%",
    height: "",
  },
});
