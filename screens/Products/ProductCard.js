import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Button,
} from "react-native";
import React from "react";

const { width } = Dimensions.get("window");
export default function ProductCard({ product }) {
  return (
    <View style={styles.rootContainer}>
      <Image
        source={{
          uri: product.image
            ? product.image
            : "https://cdn.pixabay.com/photo/2016/01/20/18/35/x-1152114__480.png",
        }}
        style={styles.image}
      />
      <View style={styles.imageContainer}>
        <Text style={styles.title}>{product.name}</Text>
        <Text style={styles.price}>{product.price}</Text>
        {product.countInStock > 0 ? (
          <Button title="add" color="green" />
        ) : (
          <Text style={{ color: "red" }}>Currently not in stock</Text>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    width: width / 2 - 20,
    height: width / 2,
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    elevation: 2,
    backgroundColor: "white",
    margin: 20,
    padding: 10,
  },
  image: {
    top: -15,
    borderRadius: 4,
    width: width / 3,
    height: "70%",
  },
  imageContainer: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 13,
    fontWeight: "bold",
    marginBottom: 8,
  },
  price: {
    fontSize: 14,
    color: "#888",
  },
});
