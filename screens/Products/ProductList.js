import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  View,
} from "react-native";
import ProductCard from "./ProductCard";

const { width } = Dimensions.get("window");
console.log(width);

function ProductList({ product }) {
  return (
    <TouchableOpacity style={styles.rootContainer}>
      <View style={styles.body}>
        <ProductCard product={product} />
      </View>
    </TouchableOpacity>
  );
}
export default ProductList;

const styles = StyleSheet.create({
  rootContainer: {
    width: "50%",
  },
  body: {
    width: width / 2,
    backgroundColor: "grey",
  },
});
