import { FlatList } from "react-native";
import { View, Text } from "react-native";
import { useState, useEffect } from "react";
import ProductList from "./ProductList";
const data = require("../../assets/data/products.json");

function ProductContainer() {
  const [products, setProduct] = useState();
  useEffect(() => {
    setProduct(data);
    return () => {
      setProduct([]);
    };
  }, []);
  return (
    <View>
      <Text> ProductContainer </Text>
      <FlatList
        numColumns={2}
        data={products}
        renderItem={({ item }) => <ProductList product={item} />}
        keyExtractor={(item) => item._id.$oid}
      />
    </View>
  );
}
export default ProductContainer;
