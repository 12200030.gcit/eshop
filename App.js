import { StyleSheet, Text, View, Dimensions } from "react-native";
import ProductContainer from "./screens/Products/ProductContainer";

const { width } = Dimensions.get("window");
export default function App() {
  return (
    <View style={styles.container}>
      <ProductContainer />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // height: width / 15.5,
    flex: 1,
    // backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
